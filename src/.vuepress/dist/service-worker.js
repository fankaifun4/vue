/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "404.html",
    "revision": "b31553b9d7f9a067a45d57771a1f91c9"
  },
  {
    "url": "api/application-api.html",
    "revision": "b43359832d28a7bc4401ea07e5eba98b"
  },
  {
    "url": "api/application-config.html",
    "revision": "8c2e34662a6a422422c4a2ac9a5931a2"
  },
  {
    "url": "api/basic-reactivity.html",
    "revision": "efc8c0109989374f265c81448a9525b0"
  },
  {
    "url": "api/built-in-components.html",
    "revision": "63c674243d94217c50f8d494d6464edf"
  },
  {
    "url": "api/composition-api.html",
    "revision": "3c9e7060f82fe1a515cb42c08a2415e8"
  },
  {
    "url": "api/computed-watch-api.html",
    "revision": "a0950cdceeda74fc9f309dfa830d9801"
  },
  {
    "url": "api/directives.html",
    "revision": "64c7b8db7b24c5e3e6122d87679d331d"
  },
  {
    "url": "api/global-api.html",
    "revision": "cee27d8c5c2b5832b9bbcf954ef8a6de"
  },
  {
    "url": "api/instance-methods.html",
    "revision": "3b5fb0b8bbab63b004c44512aef627e4"
  },
  {
    "url": "api/instance-properties.html",
    "revision": "ec8a386afc5e9937e786b1e7eb42d80b"
  },
  {
    "url": "api/options-assets.html",
    "revision": "7c57b7f578a44fde64943bb8af3808b3"
  },
  {
    "url": "api/options-composition.html",
    "revision": "0c8e1ce79f1dbe090b44ea0400a6d9ce"
  },
  {
    "url": "api/options-data.html",
    "revision": "63a6f715b5c63d96e8f0e6cf211d59be"
  },
  {
    "url": "api/options-dom.html",
    "revision": "b050822b8c3391834c45cf29d64c946c"
  },
  {
    "url": "api/options-lifecycle-hooks.html",
    "revision": "013b9f9365f702af91030807e2d250d0"
  },
  {
    "url": "api/options-misc.html",
    "revision": "6d00027bd9fd302432738ea9368fd711"
  },
  {
    "url": "api/refs-api.html",
    "revision": "4599907e9466dc58b9950a0a6a364776"
  },
  {
    "url": "api/special-attributes.html",
    "revision": "c0420439a90ce129f5563326a6a925f5"
  },
  {
    "url": "assets/css/0.styles.37d7dfb3.css",
    "revision": "6dbcd7cde56d405e9e420ab1ce01d55e"
  },
  {
    "url": "assets/img/search.83621669.svg",
    "revision": "83621669651b9a3d4bf64d1a670ad856"
  },
  {
    "url": "assets/js/1.2a70387c.js",
    "revision": "62eaeef66a1de0b67aecd8295dc14e7a"
  },
  {
    "url": "assets/js/10.4a3104a9.js",
    "revision": "2a7b5b81138c126469ef02d0ff36540e"
  },
  {
    "url": "assets/js/100.626bc553.js",
    "revision": "772798de9555156d923543df9986164e"
  },
  {
    "url": "assets/js/101.afacfc48.js",
    "revision": "86293c43a3db5e1ece0eb275557b8567"
  },
  {
    "url": "assets/js/102.e6923ce3.js",
    "revision": "ee6128b1001ea367cb2f5ec9484ed2c4"
  },
  {
    "url": "assets/js/103.52c9f1c9.js",
    "revision": "b75de11d2fd8275613138ec668ff66ac"
  },
  {
    "url": "assets/js/104.0bc3720c.js",
    "revision": "bc115b4e301ca899450daa2d1fa90092"
  },
  {
    "url": "assets/js/105.bb4a0afb.js",
    "revision": "c0db1872a3618b02dc6975a566586f7f"
  },
  {
    "url": "assets/js/106.b0a9dc2d.js",
    "revision": "f4a645ab360eabced2742ef9c6144c20"
  },
  {
    "url": "assets/js/107.860498c4.js",
    "revision": "78feca1d81d8caa2289c86e4b9f93fab"
  },
  {
    "url": "assets/js/108.e1e86264.js",
    "revision": "974aab07fe0e8443b77a0be5140dc9a8"
  },
  {
    "url": "assets/js/109.001ba850.js",
    "revision": "de63503a5bac05e4548fb36e657c69af"
  },
  {
    "url": "assets/js/11.4d045ba7.js",
    "revision": "882da5947a457cf78dc17f258e856827"
  },
  {
    "url": "assets/js/110.6c0c4c23.js",
    "revision": "7b358d2d1bbf3f687e3fb75cc55feb28"
  },
  {
    "url": "assets/js/111.bcfdca18.js",
    "revision": "bfdf08ac8d249c9739f643f93cb05fe8"
  },
  {
    "url": "assets/js/112.daac9dc5.js",
    "revision": "a4c46195310695737a6a29b61bdd8171"
  },
  {
    "url": "assets/js/113.18675c1b.js",
    "revision": "e5f45f2728d47e9af6209489a795e7b8"
  },
  {
    "url": "assets/js/114.1f981ad8.js",
    "revision": "5cad241d380b5eae36d4b241bae844f0"
  },
  {
    "url": "assets/js/115.589101e0.js",
    "revision": "09686a87be7334c3144b2256de290225"
  },
  {
    "url": "assets/js/116.d0fe6e0b.js",
    "revision": "3611885ac1810a8fb459b9f53524cd22"
  },
  {
    "url": "assets/js/117.19494a6f.js",
    "revision": "6e35dff595aaee481f986237c3519f63"
  },
  {
    "url": "assets/js/118.0f272579.js",
    "revision": "4fe3d335ff45c174309f9568f9490b36"
  },
  {
    "url": "assets/js/119.e4fe8ea7.js",
    "revision": "ffc0f11daf351afeff016126ca664e61"
  },
  {
    "url": "assets/js/12.00cfaf54.js",
    "revision": "570a0e510a66424b00cfd0dcc3e10ef1"
  },
  {
    "url": "assets/js/120.852b7c29.js",
    "revision": "993e2ab6e174bec34e2e262677b6b200"
  },
  {
    "url": "assets/js/121.65a0db30.js",
    "revision": "8c901f7e822d5038642a2a40974df531"
  },
  {
    "url": "assets/js/122.082323ab.js",
    "revision": "9dd567ac1e8ec0f099e9bbd944c73596"
  },
  {
    "url": "assets/js/123.41b9d259.js",
    "revision": "afca0d3b102139e8aca7d4dfeed3a4a0"
  },
  {
    "url": "assets/js/124.b9b8c5e5.js",
    "revision": "d1f3a34bc010fe45f895b5abf127d212"
  },
  {
    "url": "assets/js/125.e600092a.js",
    "revision": "50ab09ca6f47b79253bd1f5d7e1972ea"
  },
  {
    "url": "assets/js/126.57e59b1f.js",
    "revision": "f97b6aa381998258b66bab552f93cb7f"
  },
  {
    "url": "assets/js/127.540a6e6b.js",
    "revision": "4e0296560c9e58e37d2abd1abb2f2d6b"
  },
  {
    "url": "assets/js/128.08c04958.js",
    "revision": "28ff5db016f3f1fc258e2c7d0c5b0b79"
  },
  {
    "url": "assets/js/129.558df77d.js",
    "revision": "0d4e0c5a56da1532b7aef7474d518e02"
  },
  {
    "url": "assets/js/13.2368252f.js",
    "revision": "4ba634e2a21db0a417a1a41e04fa20a8"
  },
  {
    "url": "assets/js/130.6a3c388c.js",
    "revision": "ac1f805b6fe920f29472e378757471d3"
  },
  {
    "url": "assets/js/131.0db9f665.js",
    "revision": "1a94c3d7d2bceca724840355f496e1b4"
  },
  {
    "url": "assets/js/132.0e2067ae.js",
    "revision": "46b6efe2756a50c80cafcd74614ddaf3"
  },
  {
    "url": "assets/js/133.4186f6bd.js",
    "revision": "4c846e4e2d1785aaf844336cb47cc090"
  },
  {
    "url": "assets/js/134.3c5e3f38.js",
    "revision": "40451accdb35801968dc83c5a26b6797"
  },
  {
    "url": "assets/js/135.73e327ea.js",
    "revision": "eb70aa82e669e07c227f754d7ffefccb"
  },
  {
    "url": "assets/js/136.5bc147f0.js",
    "revision": "f0fd08e5c17391857b9c580ac160f080"
  },
  {
    "url": "assets/js/137.3c88bbaf.js",
    "revision": "8551d389cd55f9be6de9f2f143a7a105"
  },
  {
    "url": "assets/js/138.47cc2526.js",
    "revision": "dbe3bfaf2581f86ef84e74489d688e2d"
  },
  {
    "url": "assets/js/139.4f5902d8.js",
    "revision": "cdaa17d9b1f9bf6aa204c858df13018a"
  },
  {
    "url": "assets/js/14.73ca31f7.js",
    "revision": "abd23155168d50c610fe28915fec3389"
  },
  {
    "url": "assets/js/140.394d3f69.js",
    "revision": "a3176e85e3348354eb108d980a57ff3c"
  },
  {
    "url": "assets/js/141.d8169fca.js",
    "revision": "b2516a94437b21b1eceba96393f46b70"
  },
  {
    "url": "assets/js/142.7b5f043e.js",
    "revision": "ea5add29ab35ba56b0c5187c87273894"
  },
  {
    "url": "assets/js/143.755bffde.js",
    "revision": "d557f16b752284e319dca41f9d73c45f"
  },
  {
    "url": "assets/js/144.ed7c65e1.js",
    "revision": "63f211d9f9d16048be18f833366a8bad"
  },
  {
    "url": "assets/js/145.7560072f.js",
    "revision": "2dbfbde64a992a47b1dceece3570b8c1"
  },
  {
    "url": "assets/js/146.9d2eee8a.js",
    "revision": "d792a5a1d53da07517f547ed76484232"
  },
  {
    "url": "assets/js/147.fc2123ba.js",
    "revision": "d0830ab4af4ba225e8bcd52ce45438c3"
  },
  {
    "url": "assets/js/148.3c045ca5.js",
    "revision": "95b533dbaf2c7e2808b36e4d753a1b91"
  },
  {
    "url": "assets/js/149.9b2c0d90.js",
    "revision": "c7995b5e810793445e9987c04afcb6d7"
  },
  {
    "url": "assets/js/15.f32e14b7.js",
    "revision": "f4cb1d45d6718a69f83a37d1d9e8cd1d"
  },
  {
    "url": "assets/js/16.19d1bbb0.js",
    "revision": "b91c1a0344dc220f054d21747ef839cf"
  },
  {
    "url": "assets/js/17.0afcdf2c.js",
    "revision": "cd2de16f6906f6e7f28cd73507d3578d"
  },
  {
    "url": "assets/js/18.013abfbc.js",
    "revision": "85e05e1d1b6c5d7ff773bca1de05a0ae"
  },
  {
    "url": "assets/js/19.7b5f6b6c.js",
    "revision": "2517f4df4ca2bd296d51741bd02d813d"
  },
  {
    "url": "assets/js/2.40676548.js",
    "revision": "343537d103b8ab64684114fcdecae790"
  },
  {
    "url": "assets/js/20.f0d8e44c.js",
    "revision": "4c4626de29fcb39c8bf3ad8f45a94c9d"
  },
  {
    "url": "assets/js/21.82b0ba68.js",
    "revision": "695f8e0ef5748a94291b6b71561a2fd4"
  },
  {
    "url": "assets/js/22.e605fb75.js",
    "revision": "2087ea55dc219c0db979c1d165a861d0"
  },
  {
    "url": "assets/js/23.c7677a9c.js",
    "revision": "415ceaf94000ea7bb9d61da5cf1bb459"
  },
  {
    "url": "assets/js/24.a96b65c9.js",
    "revision": "c09b6a38377ab0a9ed72be2a1294381f"
  },
  {
    "url": "assets/js/25.b4a4bfb5.js",
    "revision": "d27ec24544da4f92a2652d51954f1fb0"
  },
  {
    "url": "assets/js/26.4eb6feb6.js",
    "revision": "4315ad238f8e42edb191541ec3ca9673"
  },
  {
    "url": "assets/js/27.bbb08da6.js",
    "revision": "fbfad7b9aa4f2ccb726155b6bb4fb6d8"
  },
  {
    "url": "assets/js/28.d4767a88.js",
    "revision": "f556bebe19a78d80e2a1f2eff4c099e9"
  },
  {
    "url": "assets/js/29.89337164.js",
    "revision": "bfa8d3da10da9970d158dfdc2e4b50ea"
  },
  {
    "url": "assets/js/3.25d49a5e.js",
    "revision": "5100297f22f4a5dd3f6034ecf186312a"
  },
  {
    "url": "assets/js/30.003b2882.js",
    "revision": "6ff8612314856e14fa491d5aea856b2e"
  },
  {
    "url": "assets/js/31.7d38f820.js",
    "revision": "69986a747a4494a051e2c90cb1d5d3d1"
  },
  {
    "url": "assets/js/32.8c746c0b.js",
    "revision": "1cc238842de2641f28b6880d87fec3c1"
  },
  {
    "url": "assets/js/33.0d697164.js",
    "revision": "173ad017de7a4bdd61eea4c63207392b"
  },
  {
    "url": "assets/js/34.f3ae7e6c.js",
    "revision": "38c46c1ed9cde664ffdc488cfd17732f"
  },
  {
    "url": "assets/js/35.4116973a.js",
    "revision": "7192b535f5fbc8a4bfce5ce8ba2b9f84"
  },
  {
    "url": "assets/js/36.007a47e9.js",
    "revision": "b9138ec454f2a48fa684c2a1e88a0634"
  },
  {
    "url": "assets/js/37.aba78da6.js",
    "revision": "ca92c707fd161bfca813d2f914847f30"
  },
  {
    "url": "assets/js/38.31802056.js",
    "revision": "94c74a74170f084660de3149c7add585"
  },
  {
    "url": "assets/js/39.c827e64c.js",
    "revision": "79f0eecbdbe2579552cb18e675afff82"
  },
  {
    "url": "assets/js/4.f2b3ce76.js",
    "revision": "be93c1145b6c6370af71d73d2fd458e4"
  },
  {
    "url": "assets/js/40.6dfcd99f.js",
    "revision": "58a94f9012aa21f038752785e147d99f"
  },
  {
    "url": "assets/js/41.41d5a4fd.js",
    "revision": "b31c03bdb59b8faac716b1b5b1b804b1"
  },
  {
    "url": "assets/js/42.b12cfb4b.js",
    "revision": "d9a0325c1d6f226f99c18ffe8e592247"
  },
  {
    "url": "assets/js/43.ba0c292f.js",
    "revision": "77a4471edb84296c50199a14342eccb1"
  },
  {
    "url": "assets/js/44.80471b5b.js",
    "revision": "7d78467f060cb15864ae79e45cb49e53"
  },
  {
    "url": "assets/js/45.b0fcda3d.js",
    "revision": "d1d21189e0367ff6a4a6de090365d8be"
  },
  {
    "url": "assets/js/46.a0ff1e1f.js",
    "revision": "e388b4e0cbcf872d9187b5d77602b157"
  },
  {
    "url": "assets/js/47.70b42a6a.js",
    "revision": "f9a24518048219028f942f35986c3b27"
  },
  {
    "url": "assets/js/48.fc41ecc0.js",
    "revision": "7b09876c093d4b427dc50cb39380e917"
  },
  {
    "url": "assets/js/49.a3daefab.js",
    "revision": "93d91b20ede0edbb322ba3cbb944e8b3"
  },
  {
    "url": "assets/js/5.bd9dc223.js",
    "revision": "4e10dc41fb8c826c3b696ded31f00f42"
  },
  {
    "url": "assets/js/50.e9db2f74.js",
    "revision": "f663298880bcaf03fb52688da4879544"
  },
  {
    "url": "assets/js/51.66fdfb4a.js",
    "revision": "dad3f5856c9c458b0a5dc8ac8756677e"
  },
  {
    "url": "assets/js/52.ae634f59.js",
    "revision": "8a76ec772a20d9c3e5af8ae2fda67759"
  },
  {
    "url": "assets/js/53.cf971023.js",
    "revision": "0040ac9122cccfb464095bea9bb5485a"
  },
  {
    "url": "assets/js/54.408ebce7.js",
    "revision": "96151ab07127d2821a48dbe9d0df2adf"
  },
  {
    "url": "assets/js/55.c7739736.js",
    "revision": "4f621b6b6a490c6033888cb25bf6452c"
  },
  {
    "url": "assets/js/56.c1a5ff89.js",
    "revision": "34cb7a695aae309a72e853afb3181948"
  },
  {
    "url": "assets/js/57.80b327f4.js",
    "revision": "231857c9ed77b5faf47753b3ec21a787"
  },
  {
    "url": "assets/js/58.d68beee1.js",
    "revision": "b0637fdb13c5e8aa8e501dfc2219f534"
  },
  {
    "url": "assets/js/59.485d7d94.js",
    "revision": "d91493d6626aae58f7b00c0d17f9b2f1"
  },
  {
    "url": "assets/js/6.79650c82.js",
    "revision": "213a8a8e0bf872e9652659a9c05343bd"
  },
  {
    "url": "assets/js/60.b9e5595f.js",
    "revision": "ebf2b342734b21ff3996d8a37765623f"
  },
  {
    "url": "assets/js/61.f4c974ed.js",
    "revision": "21a98bcca925fc5d9d51e89cf0e6b08a"
  },
  {
    "url": "assets/js/62.3b385580.js",
    "revision": "be038b5feaa6192486a7b11b317ed25c"
  },
  {
    "url": "assets/js/63.06a1fcde.js",
    "revision": "8d61381e0f723f4375ff22a1bcec3bd9"
  },
  {
    "url": "assets/js/64.658dbb65.js",
    "revision": "005b4d011ce31e2d682ef844cfe1941c"
  },
  {
    "url": "assets/js/65.2d1a7388.js",
    "revision": "15555395c13228bb3607e41bae9eb61c"
  },
  {
    "url": "assets/js/66.9af827f6.js",
    "revision": "b53a253f691b7a49bde39ddeb5c6c42b"
  },
  {
    "url": "assets/js/67.77ce048a.js",
    "revision": "ce60f83a7ad5e04adbff465960d0980c"
  },
  {
    "url": "assets/js/68.ae9ec5dd.js",
    "revision": "294d8de07b29ba466a44fc729bf1b89f"
  },
  {
    "url": "assets/js/69.2488bf3d.js",
    "revision": "42131167a3e3540bfcd5cfacd18e6029"
  },
  {
    "url": "assets/js/70.cd0bef0c.js",
    "revision": "b0f96bb60cff36f94a605e403aedd82e"
  },
  {
    "url": "assets/js/71.7bac6296.js",
    "revision": "6baf7533d7124505442434d801569eac"
  },
  {
    "url": "assets/js/72.65e5bc3a.js",
    "revision": "4f1ac66381081d64829213350d0a195d"
  },
  {
    "url": "assets/js/73.94ad3304.js",
    "revision": "76c6240df9f0699c95aef0543112baf2"
  },
  {
    "url": "assets/js/74.16e8cf14.js",
    "revision": "50a2c96b7391811b8002941e07408233"
  },
  {
    "url": "assets/js/75.091ae780.js",
    "revision": "324465bd7a09d8dad2b84ea81d5be545"
  },
  {
    "url": "assets/js/76.b93910e9.js",
    "revision": "6575016c3f7da2122f30e62d57899aed"
  },
  {
    "url": "assets/js/77.39e6e942.js",
    "revision": "527262b5f9ea050aedb993f88144d6d4"
  },
  {
    "url": "assets/js/78.f34ee602.js",
    "revision": "5d43fc4719da9b7ad1ced3061ee0369e"
  },
  {
    "url": "assets/js/79.788df69c.js",
    "revision": "64368c41cc4bfc021b0b255fc0ed1983"
  },
  {
    "url": "assets/js/80.08af1291.js",
    "revision": "4d2ceaaa11bf0b4e8f6af3d892fcb20e"
  },
  {
    "url": "assets/js/81.676a7dcc.js",
    "revision": "03b2a538385b1078db6b99f2f6356a1b"
  },
  {
    "url": "assets/js/82.8377221b.js",
    "revision": "3b17782c936c73919b18add36f9c0b5a"
  },
  {
    "url": "assets/js/83.d6931507.js",
    "revision": "47cb24988bc268b5114f15f15ae632f1"
  },
  {
    "url": "assets/js/84.e68f3678.js",
    "revision": "ff20abadcf82c3bc72dd80b3b712fe16"
  },
  {
    "url": "assets/js/85.a1c6ef06.js",
    "revision": "a59bb1167bf3978cf31197e244dc54d0"
  },
  {
    "url": "assets/js/86.376ac840.js",
    "revision": "070cc8b122154d9fc4992638a2c9a7b6"
  },
  {
    "url": "assets/js/87.6d85b18f.js",
    "revision": "0cfb8662b7a78178a2dbd3b13b4e458b"
  },
  {
    "url": "assets/js/88.c4b36e3f.js",
    "revision": "a40eb2c37bf82b6dca66d9b1960d6236"
  },
  {
    "url": "assets/js/89.76835629.js",
    "revision": "26b6b21aa81672c848510617c6775aef"
  },
  {
    "url": "assets/js/9.1a6d3870.js",
    "revision": "16765b26c36666100b00941a134cb937"
  },
  {
    "url": "assets/js/90.26dc881b.js",
    "revision": "f72c153ff0e939067149cebdd072519f"
  },
  {
    "url": "assets/js/91.162c41ae.js",
    "revision": "30a9b888c912cb9e7165f1978d7fe97a"
  },
  {
    "url": "assets/js/92.4d642c16.js",
    "revision": "2733ad4da4993298e9ed986e6f8ca0a7"
  },
  {
    "url": "assets/js/93.6678e584.js",
    "revision": "d9a1f998b8e1ae58b60796bcc4e7932e"
  },
  {
    "url": "assets/js/94.904c1358.js",
    "revision": "b7ee833b3d3299ad8a7689e576d46b65"
  },
  {
    "url": "assets/js/95.3eb77577.js",
    "revision": "5897f0e19f02b451a9fd9bfb2c6e81c3"
  },
  {
    "url": "assets/js/96.23d3b20c.js",
    "revision": "80cb81f80b6377eba5f08278879d2a98"
  },
  {
    "url": "assets/js/97.62741dda.js",
    "revision": "294565c22127b7f793fa345321bda13e"
  },
  {
    "url": "assets/js/98.cc4208a4.js",
    "revision": "1d52244b1b15476e927857becf10071e"
  },
  {
    "url": "assets/js/99.ffd4c8f3.js",
    "revision": "5163b5b4bc75df3b20085563524ca7d4"
  },
  {
    "url": "assets/js/app.c9ff6c3e.js",
    "revision": "1f53fcdd353ecd8a6901b2fbcc0d8862"
  },
  {
    "url": "assets/js/vendors~docsearch.3f439199.js",
    "revision": "efc7761cc00c123012d5c9244efce190"
  },
  {
    "url": "coc/index.html",
    "revision": "7576f84d58021d4bc4e42b15f40b7be4"
  },
  {
    "url": "community/join.html",
    "revision": "3af02b1066f1f18ed522699671d3f6e0"
  },
  {
    "url": "community/partners.html",
    "revision": "124ea3e8e49cc519c3e433143108692e"
  },
  {
    "url": "community/team.html",
    "revision": "c6e6e68fc12546f74655bd334c79a81f"
  },
  {
    "url": "community/themes.html",
    "revision": "8cc908b1dab4f0e5f51d7dcff19e6ee3"
  },
  {
    "url": "cookbook/editable-svg-icons.html",
    "revision": "461bbabb73243caf553c10ef9cdba865"
  },
  {
    "url": "cookbook/index.html",
    "revision": "1fd2d8aed01347d5516dad4d0cb3b0d0"
  },
  {
    "url": "examples/commits.html",
    "revision": "43c052b6e809664a8ff6d8f7d44d3e1b"
  },
  {
    "url": "examples/elastic-header.html",
    "revision": "74316f9cb8c6595136fbeaa85955f6f5"
  },
  {
    "url": "examples/grid-component.html",
    "revision": "8f2f579af4fc36ac5eb0b812400a8c4e"
  },
  {
    "url": "examples/markdown.html",
    "revision": "7696b5129eda526a95c1ae586f65fd39"
  },
  {
    "url": "examples/modal.html",
    "revision": "17d32d408f1c23493db8ecc75c092e94"
  },
  {
    "url": "examples/select2.html",
    "revision": "56e8437f9c16bab5ca47a46dc2bc9bd8"
  },
  {
    "url": "examples/svg.html",
    "revision": "bc2ad5d02cbf7b995f47858d959cee34"
  },
  {
    "url": "examples/todomvc.html",
    "revision": "9810e6d253126d6ee195c7f6bcb13d52"
  },
  {
    "url": "examples/tree-view.html",
    "revision": "b4733bf8b7aba3cc4066cb6b48617744"
  },
  {
    "url": "guide/a11y-basics.html",
    "revision": "a02eb429bc56cebd832fa89e9dce3056"
  },
  {
    "url": "guide/a11y-resources.html",
    "revision": "f3d287ee25e508d7028b1143e7772be1"
  },
  {
    "url": "guide/a11y-semantics.html",
    "revision": "9b21483dffa96b97b00e82fb43d48ba7"
  },
  {
    "url": "guide/a11y-standards.html",
    "revision": "0ea00fcdd9f9ca393ad9e46d29a3f900"
  },
  {
    "url": "guide/change-detection.html",
    "revision": "f542dc723ae4652395aac9919d9c1e99"
  },
  {
    "url": "guide/class-and-style.html",
    "revision": "1d02b28d202bd2c8edd1b813b1f2a1a0"
  },
  {
    "url": "guide/component-attrs.html",
    "revision": "f44582edbebb82aeb945adf056c3ff52"
  },
  {
    "url": "guide/component-basics.html",
    "revision": "f4d56d4200358bb85dd9c37cd2f0a54a"
  },
  {
    "url": "guide/component-custom-events.html",
    "revision": "a76feb3ffca092477a98fe0cfb35716f"
  },
  {
    "url": "guide/component-dynamic-async.html",
    "revision": "2bb790b4542a5b42296bb9e437be40e8"
  },
  {
    "url": "guide/component-edge-cases.html",
    "revision": "ffa4579430766d8d909ae079afddaafc"
  },
  {
    "url": "guide/component-props.html",
    "revision": "edd454646466b7b984a4449111ae734d"
  },
  {
    "url": "guide/component-provide-inject.html",
    "revision": "772551a4012e83c53d7a4850a27b9694"
  },
  {
    "url": "guide/component-registration.html",
    "revision": "deac91b8f4f08abdf869e5e7dae40aba"
  },
  {
    "url": "guide/component-slots.html",
    "revision": "fb735cd5b1539645e085651c62227991"
  },
  {
    "url": "guide/component-template-refs.html",
    "revision": "a1f389775526f83806f26490404a7454"
  },
  {
    "url": "guide/composition-api-introduction.html",
    "revision": "0a39b42a9fd4e0e45d1f819c0825e809"
  },
  {
    "url": "guide/composition-api-lifecycle-hooks.html",
    "revision": "8dd7f7b3fe1bb308e2ec1b7cc66aec1b"
  },
  {
    "url": "guide/composition-api-provide-inject.html",
    "revision": "44668f30976e5d6fe3411bf34e9ec6b7"
  },
  {
    "url": "guide/composition-api-setup.html",
    "revision": "331964e87b99f174dd677e297fe63532"
  },
  {
    "url": "guide/composition-api-template-refs.html",
    "revision": "131b4daa82f22cf821d80e2c22ae6be9"
  },
  {
    "url": "guide/computed.html",
    "revision": "76d2299693568249de7bd21728a2d33b"
  },
  {
    "url": "guide/conditional.html",
    "revision": "999624459fe13a7ee3b4fe32dcfe0643"
  },
  {
    "url": "guide/contributing/doc-style-guide.html",
    "revision": "bda083b1b13672f05f41e221c0311fb6"
  },
  {
    "url": "guide/contributing/translations.html",
    "revision": "2c9e78b4bb6ebae48bc678760d27aa04"
  },
  {
    "url": "guide/contributing/writing-guide.html",
    "revision": "7bebb311e9f22e4d206ff9bb39a6e701"
  },
  {
    "url": "guide/custom-directive.html",
    "revision": "7e9043ed1ce380dc853c3560af2a3a3d"
  },
  {
    "url": "guide/events.html",
    "revision": "c5f36248ffe0d75c6065d65e679b6b2d"
  },
  {
    "url": "guide/forms.html",
    "revision": "d8abbde0a83eb5958f99c7d244c16940"
  },
  {
    "url": "guide/installation.html",
    "revision": "e353369f150c45297c895f7b52e258ed"
  },
  {
    "url": "guide/instance.html",
    "revision": "3f17a85dc0c3cf9688114d6cd6553a4f"
  },
  {
    "url": "guide/introduction.html",
    "revision": "6e3f0ac8160502a521eb018a89d02506"
  },
  {
    "url": "guide/list.html",
    "revision": "020a60b5a61f90b4efc369f64730634f"
  },
  {
    "url": "guide/migration/array-refs.html",
    "revision": "5a9cdeb2eb1f51163d9b7365be94481b"
  },
  {
    "url": "guide/migration/async-components.html",
    "revision": "0d395a28d41ffb8ba1c193a72619434b"
  },
  {
    "url": "guide/migration/attribute-coercion.html",
    "revision": "d928daf4c65d744f9b425b88c86f4966"
  },
  {
    "url": "guide/migration/custom-directives.html",
    "revision": "5d11f7254462bc08572e89ea5f351ad9"
  },
  {
    "url": "guide/migration/custom-elements-interop.html",
    "revision": "d290235b30f93953a9dba972c6c91fe5"
  },
  {
    "url": "guide/migration/data-option.html",
    "revision": "ff798ac2a192ecfbd29c291e8420092a"
  },
  {
    "url": "guide/migration/events-api.html",
    "revision": "819efbd72d4acc57f92b6c67385a0340"
  },
  {
    "url": "guide/migration/filters.html",
    "revision": "75ae43fce5d622a829ed9b3ead99327e"
  },
  {
    "url": "guide/migration/fragments.html",
    "revision": "588ddd086592fb5e22485c9f23ec1e9d"
  },
  {
    "url": "guide/migration/functional-components.html",
    "revision": "9a522e4285b019a4e17f4982dc39f0d8"
  },
  {
    "url": "guide/migration/global-api-treeshaking.html",
    "revision": "42803b774d50e396dee75e267e36c7d8"
  },
  {
    "url": "guide/migration/global-api.html",
    "revision": "4bf21782d4bcada938ffb9272cabf4e6"
  },
  {
    "url": "guide/migration/inline-template-attribute.html",
    "revision": "1429f790da1719aa5442d24e15ddf3ee"
  },
  {
    "url": "guide/migration/introduction.html",
    "revision": "e09e090c3ec36b90ca2e2060617365b0"
  },
  {
    "url": "guide/migration/key-attribute.html",
    "revision": "c244320c4ed1c22c7497ec3ec676c380"
  },
  {
    "url": "guide/migration/keycode-modifiers.html",
    "revision": "fc0d64bb05b54160c20255eec6bc0451"
  },
  {
    "url": "guide/migration/props-default-this.html",
    "revision": "3a44bd6e5b6320afc4148dfa341a8c4b"
  },
  {
    "url": "guide/migration/render-function-api.html",
    "revision": "39ce4bb1c2b61fe3dab71f3278f92ff1"
  },
  {
    "url": "guide/migration/slots-unification.html",
    "revision": "13945019dada44d8fe87b3e4db458519"
  },
  {
    "url": "guide/migration/transition.html",
    "revision": "564f03d71ca960ac831f23ec2289fe04"
  },
  {
    "url": "guide/migration/v-bind.html",
    "revision": "2602c867d81a9d64839bb21cf1b0a668"
  },
  {
    "url": "guide/migration/v-if-v-for.html",
    "revision": "ee6e94a9fceb3e7c92962359e6274f75"
  },
  {
    "url": "guide/migration/v-model.html",
    "revision": "1bf08f975cfe815534f33a1c0ac9d94d"
  },
  {
    "url": "guide/migration/watch.html",
    "revision": "64a9abc04987cd08a48dacd404eaa301"
  },
  {
    "url": "guide/mixins.html",
    "revision": "bff4b1cf3a690fcf377a37725c7dde8e"
  },
  {
    "url": "guide/mobile.html",
    "revision": "c514a8a93a225f7600d469e919d6062c"
  },
  {
    "url": "guide/optimizations.html",
    "revision": "416aae1a8895aac939fb7666cd8346a1"
  },
  {
    "url": "guide/plugins.html",
    "revision": "68d1b40835813e7628a85fe69c81d9a7"
  },
  {
    "url": "guide/reactivity-computed-watchers.html",
    "revision": "911a7075b9142ded38c8a7104e2c2c7c"
  },
  {
    "url": "guide/reactivity-fundamentals.html",
    "revision": "f9a391bae8be6675f6c74217bf07fedc"
  },
  {
    "url": "guide/reactivity.html",
    "revision": "36804629a52f6da45109469a062ca264"
  },
  {
    "url": "guide/render-function.html",
    "revision": "d2f97bdcaf6f8b6bab1d312147b5dbcb"
  },
  {
    "url": "guide/routing.html",
    "revision": "9574c837793da0a9d9d75871d570e995"
  },
  {
    "url": "guide/single-file-component.html",
    "revision": "c3ad9e56defacfbdca1b319c2bfe0470"
  },
  {
    "url": "guide/ssr.html",
    "revision": "6f64a881213fe6b83262c2e7d2f06616"
  },
  {
    "url": "guide/state-management.html",
    "revision": "05d13f33fe5d76fd97ed4fde6f3ece9f"
  },
  {
    "url": "guide/teleport.html",
    "revision": "275681d2088244fc2bcb7c45bff13df9"
  },
  {
    "url": "guide/template-syntax.html",
    "revision": "3a8ca70e63a1a70ab51d3b1caf76ac17"
  },
  {
    "url": "guide/testing.html",
    "revision": "596d10f36a45eea12f7b6a17f0a3c08f"
  },
  {
    "url": "guide/transitions-enterleave.html",
    "revision": "f2e50fb42f9536497850d93354cba0ec"
  },
  {
    "url": "guide/transitions-list.html",
    "revision": "841214f9a76c5de90cc67554ffd7e656"
  },
  {
    "url": "guide/transitions-overview.html",
    "revision": "1b20e193aabfee3c2515fbd22f5fc92f"
  },
  {
    "url": "guide/transitions-state.html",
    "revision": "985bc5061bbe3d5ea369d9eed3714492"
  },
  {
    "url": "guide/typescript-support.html",
    "revision": "34654c95b8f2ca020f5b3a8ba8806cd2"
  },
  {
    "url": "images/AccessibilityChromeDeveloperTools.png",
    "revision": "25c2a61b52ea8753aa4693a16abaa43f"
  },
  {
    "url": "images/AccessibleARIAdescribedby.png",
    "revision": "d2b26eb9ae0006509801691c289a86d3"
  },
  {
    "url": "images/AccessibleARIAlabelDevTools.png",
    "revision": "05cb34b380cef9627d5c9a3c0ba64dca"
  },
  {
    "url": "images/AccessibleARIAlabelledbyDevTools.png",
    "revision": "1554e00985256ca1042caffbf59709cc"
  },
  {
    "url": "images/AccessibleLabelChromeDevTools.png",
    "revision": "5b9d491c368093887624f4dfacdb6431"
  },
  {
    "url": "images/components_provide.png",
    "revision": "f7110a1bae2d0744997012ca656d8fa1"
  },
  {
    "url": "images/components.png",
    "revision": "b5c08269dfc26ae6d7db3801e9efd296"
  },
  {
    "url": "images/css-vs-js-ease.svg",
    "revision": "c8b731ee9af820de36c31eaeb4e5dd2e"
  },
  {
    "url": "images/dom-tree.png",
    "revision": "f70b86bfbbfe1962dc5d6125105f1122"
  },
  {
    "url": "images/icons/android-icon-144x144.png",
    "revision": "e67b8d54852c2fbf40be2a8eb0590f5b"
  },
  {
    "url": "images/icons/android-icon-192x192.png",
    "revision": "5d10eaab941eb596ee59ffc53652d035"
  },
  {
    "url": "images/icons/android-icon-36x36.png",
    "revision": "bb757d234def1a6b53d793dbf4879578"
  },
  {
    "url": "images/icons/android-icon-48x48.png",
    "revision": "0d33c4fc51e2bb020bf8dd7cd05db875"
  },
  {
    "url": "images/icons/android-icon-72x72.png",
    "revision": "702c4fafca31d670f9bd8b2d185ced39"
  },
  {
    "url": "images/icons/android-icon-96x96.png",
    "revision": "0ebff702851985ea6ba891cf6e6e7ddd"
  },
  {
    "url": "images/icons/apple-icon-114x114.png",
    "revision": "f4fd30f3a26b932843b8c5cef9f2186e"
  },
  {
    "url": "images/icons/apple-icon-120x120.png",
    "revision": "b6a574d63d52ef9c89189b67bcac5cbd"
  },
  {
    "url": "images/icons/apple-icon-144x144.png",
    "revision": "e67b8d54852c2fbf40be2a8eb0590f5b"
  },
  {
    "url": "images/icons/apple-icon-152x152.png",
    "revision": "f53787bf41febf2b044931a305ccaf2a"
  },
  {
    "url": "images/icons/apple-icon-180x180.png",
    "revision": "9f6b1e3b92b2c5bd5b7d79501bb6e612"
  },
  {
    "url": "images/icons/apple-icon-57x57.png",
    "revision": "83f622ba0994866abc56ace068b6551c"
  },
  {
    "url": "images/icons/apple-icon-60x60.png",
    "revision": "643f761bc39f86c70f17cd1fed3b8e08"
  },
  {
    "url": "images/icons/apple-icon-72x72.png",
    "revision": "702c4fafca31d670f9bd8b2d185ced39"
  },
  {
    "url": "images/icons/apple-icon-76x76.png",
    "revision": "94d9af047b86d99657b5efb88a0d1c7b"
  },
  {
    "url": "images/icons/apple-icon-precomposed.png",
    "revision": "707758f591323153a4f1cb3a8d9641fa"
  },
  {
    "url": "images/icons/apple-icon.png",
    "revision": "707758f591323153a4f1cb3a8d9641fa"
  },
  {
    "url": "images/icons/bacancy_technology.png",
    "revision": "5810bb8253b1e35ba437373ff83f82d3"
  },
  {
    "url": "images/icons/bulb.svg",
    "revision": "e3fbc07fc8a4fd64bf3585de3c5868c4"
  },
  {
    "url": "images/icons/danger.svg",
    "revision": "7224901b2f84983efa258490e5a1c03a"
  },
  {
    "url": "images/icons/favicon-16x16.png",
    "revision": "a5a9da66870189b0539223c38c8a7749"
  },
  {
    "url": "images/icons/favicon-32x32.png",
    "revision": "3d60db0d77303b2414ddd50cf2472bf7"
  },
  {
    "url": "images/icons/favicon-96x96.png",
    "revision": "0ebff702851985ea6ba891cf6e6e7ddd"
  },
  {
    "url": "images/icons/info.svg",
    "revision": "39d1a12b97555d796d4dcda9bf403429"
  },
  {
    "url": "images/icons/ms-icon-144x144.png",
    "revision": "e67b8d54852c2fbf40be2a8eb0590f5b"
  },
  {
    "url": "images/icons/ms-icon-150x150.png",
    "revision": "e8cdf492981122a2548bc247c7b4067d"
  },
  {
    "url": "images/icons/ms-icon-310x310.png",
    "revision": "1721f8303ec2349002b5980a01f27cef"
  },
  {
    "url": "images/icons/ms-icon-70x70.png",
    "revision": "a110cf0132b00b23a8605ca72a8874ba"
  },
  {
    "url": "images/icons/stop.svg",
    "revision": "7d577a225ddf7f95f96032b3eefb48b6"
  },
  {
    "url": "images/imooc-ad3.png",
    "revision": "a8b8084e0bb616cef5637f589d0c3a49"
  },
  {
    "url": "images/lifecycle.png",
    "revision": "55ca3bcd54e2ee9bd7e3575eb02a1e13"
  },
  {
    "url": "images/partners/monterail.png",
    "revision": "db165491374f80cc4f3190a0ebd918ad"
  },
  {
    "url": "images/partners/vehikl.png",
    "revision": "65f4ae56972001f689053fba43129433"
  },
  {
    "url": "images/paypal.png",
    "revision": "067bd556ce9e4c76538a8057adb6d596"
  },
  {
    "url": "images/scoped-slot.png",
    "revision": "c6ef14ba02eac288245c5c5009d966cc"
  },
  {
    "url": "images/sfc-with-preprocessors.png",
    "revision": "2e57ecfafeac2237d5a003ad9a0ef7bc"
  },
  {
    "url": "images/sfc.png",
    "revision": "e333ce3bf8119bef381ac7c7b2bbd4ba"
  },
  {
    "url": "images/slot.png",
    "revision": "00cf6bd787014eb22b2821d72b80212a"
  },
  {
    "url": "images/sponsors/autocode.svg",
    "revision": "e1c1c17d96d289b20b2d91819a4c9e4d"
  },
  {
    "url": "images/sponsors/bacancy_technology.png",
    "revision": "9a0590eb4ce29289b454240415611162"
  },
  {
    "url": "images/sponsors/bestvpn_co.png",
    "revision": "afbe252b6b59bc3cdac2e7dec69eac39"
  },
  {
    "url": "images/sponsors/bit.png",
    "revision": "9638a3f44bf471876effb80ea0659f73"
  },
  {
    "url": "images/sponsors/chaitin.png",
    "revision": "549e43997790dc624c477424acbfe228"
  },
  {
    "url": "images/sponsors/cloudstudio.png",
    "revision": "fc480cf4c2b06591f58e7e91666226af"
  },
  {
    "url": "images/sponsors/dcloud.gif",
    "revision": "ade7c64e66506b6cff10292efea16ee8"
  },
  {
    "url": "images/sponsors/devexpress.png",
    "revision": "a6d9c786a373088c8d238ca643293c17"
  },
  {
    "url": "images/sponsors/devsquad.png",
    "revision": "e639ea4fd0d7053fc0928d4ff9fefb2a"
  },
  {
    "url": "images/sponsors/fastcoding_inc.png",
    "revision": "08a0a7652db79fa3395c0ef28d49f0cd"
  },
  {
    "url": "images/sponsors/fastcoding_inc.svg",
    "revision": "9d33d7905c4fb224aba61de096505794"
  },
  {
    "url": "images/sponsors/firestick_tricks.png",
    "revision": "1ee05223a5b12fe910cb8428d57223d8"
  },
  {
    "url": "images/sponsors/flatlogic_templates.svg",
    "revision": "4442dca91b270a32353ee5aca44ebc33"
  },
  {
    "url": "images/sponsors/foo.png",
    "revision": "1c9cde53bb9c98a316edc93d57684e4d"
  },
  {
    "url": "images/sponsors/frontend_love.png",
    "revision": "b514babc53a0f3ddc854b0b14a54a489"
  },
  {
    "url": "images/sponsors/frontendlove.png",
    "revision": "b514babc53a0f3ddc854b0b14a54a489"
  },
  {
    "url": "images/sponsors/gridsome.png",
    "revision": "e82a2f872ec319bbb5d0a804288cd9b7"
  },
  {
    "url": "images/sponsors/happy_programmer_llc.png",
    "revision": "3f3303d42a57ff9edf36373f59d376af"
  },
  {
    "url": "images/sponsors/html_burger.png",
    "revision": "c7ce1344d001e7a236a89694ed59d988"
  },
  {
    "url": "images/sponsors/icons_8.png",
    "revision": "ffcdd01817ecdb32b92bd2f1e4d75e84"
  },
  {
    "url": "images/sponsors/icons.png",
    "revision": "ad6ee8c400066e15712cdef4342023da"
  },
  {
    "url": "images/sponsors/imooc-sponsor.png",
    "revision": "7ddc7f938fbbc08f816a888225786a4c"
  },
  {
    "url": "images/sponsors/imooc-sponsor2.png",
    "revision": "ce9575f62520e0ac8b7d93ada2c6b274"
  },
  {
    "url": "images/sponsors/inkoop.png",
    "revision": "1cff77d2c927657d3aceeba2c12db892"
  },
  {
    "url": "images/sponsors/intygrate.png",
    "revision": "fdd390b44a4aeed763f53f4e8f6529e4"
  },
  {
    "url": "images/sponsors/isolutions_uk_limited.png",
    "revision": "0f76512940c38b72fcf48337b4d64692"
  },
  {
    "url": "images/sponsors/laravel.png",
    "revision": "9a2fba3eca41e26743dc731e3a4469b6"
  },
  {
    "url": "images/sponsors/modus.png",
    "revision": "6498c04fee5b8542449b350e77180379"
  },
  {
    "url": "images/sponsors/Monterail.png",
    "revision": "bf1ec94a0ca48f0e6be0c97fa60a42c0"
  },
  {
    "url": "images/sponsors/moovweb.png",
    "revision": "8183954731fdeb0f136fac1485198184"
  },
  {
    "url": "images/sponsors/neds.png",
    "revision": "1f1a2a46c2575019ae07a90205f60b65"
  },
  {
    "url": "images/sponsors/onsen_ui.png",
    "revision": "e41569bcb10fbca3f361d818b29ed7fd"
  },
  {
    "url": "images/sponsors/passionate_people.png",
    "revision": "03e59e28347e1dcd165e4e1525afb545"
  },
  {
    "url": "images/sponsors/primevue copy.png",
    "revision": "60f2e8fb0dce3e9045fc3a2a8039fa82"
  },
  {
    "url": "images/sponsors/primevue.png",
    "revision": "60f2e8fb0dce3e9045fc3a2a8039fa82"
  },
  {
    "url": "images/sponsors/programmers_io.png",
    "revision": "02cb415eb9a8e9ce6579c7aff03759dd"
  },
  {
    "url": "images/sponsors/pullrequest.svg",
    "revision": "50847513b306736d33234d50b11c5e1d"
  },
  {
    "url": "images/sponsors/retool.png",
    "revision": "aaad6a749deb625da5771750dcb51920"
  },
  {
    "url": "images/sponsors/roadster.png",
    "revision": "080fb711e736d686f182358a582d7c6b"
  },
  {
    "url": "images/sponsors/shopware_ag.png",
    "revision": "e2ded483c0660bd629938e37f388d9fb"
  },
  {
    "url": "images/sponsors/storekit.png",
    "revision": "cacf47116e5efe9fc2dcd60ebc197707"
  },
  {
    "url": "images/sponsors/storyblok.png",
    "revision": "64ec1772109b769e91138b58526484ad"
  },
  {
    "url": "images/sponsors/tidelift.png",
    "revision": "831935bd53d7d2d4eea9427c5f874816"
  },
  {
    "url": "images/sponsors/tighten_co.png",
    "revision": "003364e7044150e2979cbfe03d640cec"
  },
  {
    "url": "images/sponsors/tooltwist.png",
    "revision": "b81bfd5ae608e965d03aaa5a4164373e"
  },
  {
    "url": "images/sponsors/unicorn.png",
    "revision": "e0c072bd78f366471a393b9c366c9b74"
  },
  {
    "url": "images/sponsors/usave.png",
    "revision": "5cffd5053b1d75ae49c9b6eb176e0ccf"
  },
  {
    "url": "images/sponsors/valuecoders.png",
    "revision": "818ca42a745e018ace0c55c36a7ae3dd"
  },
  {
    "url": "images/sponsors/vehikl.png",
    "revision": "3bd1b88aa9d242d308e838f2342d7660"
  },
  {
    "url": "images/sponsors/vpnranks.png",
    "revision": "35d7392e773d487e13358d8b5f7fb646"
  },
  {
    "url": "images/sponsors/vuejobs.png",
    "revision": "77ed618e17571d1a2d77ad7bc53e8fc4"
  },
  {
    "url": "images/sponsors/vuemastery.png",
    "revision": "6f09ce143467fba22039bde3f2070c19"
  },
  {
    "url": "images/sponsors/vueschool.png",
    "revision": "3d92b4f1a8fcbe3be0d0e89950a1a705"
  },
  {
    "url": "images/sponsors/vuetify.png",
    "revision": "c7cfff77abb10162cb0b7c2ed3b6ac51"
  },
  {
    "url": "images/sponsors/watchcartoononline.png",
    "revision": "f7cf1082b14003908496f02f9eb2ae00"
  },
  {
    "url": "images/sponsors/webdock.png",
    "revision": "6b8d3d271ba4d05daf83ad75d21221d1"
  },
  {
    "url": "images/sponsors/webucator.png",
    "revision": "3c87885f4c36bc1b07f8c2b547e84b6f"
  },
  {
    "url": "images/sponsors/wilderminds.png",
    "revision": "cd98b69653b51369da2e765097f13d6f"
  },
  {
    "url": "images/sponsors/writers_per_hour.jpg",
    "revision": "2033e6d7e88969e97e78e38d8d030eb9"
  },
  {
    "url": "images/sponsors/x_team.png",
    "revision": "a6cfaebb0c0dc17d348bc9c6fd5758ef"
  },
  {
    "url": "images/sponsors/y8.png",
    "revision": "3cdd8826d3419751f40a8aa7f90cd539"
  },
  {
    "url": "images/sponsors/yakaz.png",
    "revision": "f1918919114e35d6091e67370450e8bd"
  },
  {
    "url": "images/sponsors/youku.png",
    "revision": "1cce2782971aed63d38b17e28614d512"
  },
  {
    "url": "images/state.png",
    "revision": "6a05b01942c7d2cff4ea0033ded59ebb"
  },
  {
    "url": "images/transition.png",
    "revision": "5990c1dff7dc7a8fb3b34b4462bd0105"
  },
  {
    "url": "images/transitions.svg",
    "revision": "f0002fdb27ed433579d15f67c3b06153"
  },
  {
    "url": "images/v-bind-instead-of-sync.png",
    "revision": "cb59705b61fd5a75b1903f6a0b497cb1"
  },
  {
    "url": "index.html",
    "revision": "46cbcfe41713c72c58ffa166fc3c85c1"
  },
  {
    "url": "logo.png",
    "revision": "cf23526f451784ff137f161b8fe18d5a"
  },
  {
    "url": "style-guide/index.html",
    "revision": "3190acd2f03ec0d3900f93e9e2ed818e"
  },
  {
    "url": "support-vuejs/index.html",
    "revision": "c240c9defddb3fa3e8f1bbd6ec2c8407"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
addEventListener('message', event => {
  const replyPort = event.ports[0]
  const message = event.data
  if (replyPort && message && message.type === 'skip-waiting') {
    event.waitUntil(
      self.skipWaiting().then(
        () => replyPort.postMessage({ error: null }),
        error => replyPort.postMessage({ error })
      )
    )
  }
})
